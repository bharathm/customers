package com.wavelabs.customers.application.model;

public class Address {

	private String address;
	private String landmark;
	private String city;
	private String state;
	private String country;

	public Address(String address, String landmark, String city, String state, String country) {
		this.address = address;
		this.landmark = landmark;
		this.city = city;
		this.state = state;
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
