package com.wavelabs.customers.application.model;

public class Customer {

	private int id;
	private String firstname;
	private String lastname;
	private String emailid;
	private Address address;

	public Customer(int id, String firstname, String lastname, String emailid, Address address) {
		this.id = id;
		this.firstname=firstname;
		this.lastname=lastname;
		this.address=address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

}
