package com.wavelabs.customers.application.service;

import org.springframework.stereotype.Service;

import com.wavelabs.customers.application.model.Address;
import com.wavelabs.customers.application.model.Customer;


@Service
public class CustomerService {

	public Customer getCustomerDetails(int id) {
		Address address = new Address("AFG2", "East Santa Clara Street", "San Jose", "California", "USA");
		Customer customer = new Customer(id, "Jhon F", "Kendy", "jhon@gmail.com", address);
		return customer;
	}

}
